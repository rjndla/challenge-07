/** @format */

import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Register from "./pages/Register";
import Home from "./pages/Home";
import Dashboard from "./pages/Dashboard";
import Login from "./pages/Login";
import AddNewCar from "./pages/AddNewCar";
import ProtectedRoute from "./routes/ProtectedRoute";
import Cars from "./pages/Cars";
import DetailCar from "./pages/DetailCar";
import SewaMobil from "./pages/SewaMobil";
import CarDetail from "./components/SewaMobil/CarDetail";
import Tiket from "./pages/Tiket";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route element={<ProtectedRoute />}>
          <Route path='/home' element={<Home />} />
          <Route path='/dashboard' element={<Dashboard />} />
          <Route path='/addCar' element={<AddNewCar />} />
          <Route path='/cars' element={<Cars />} />
          <Route path='/cars/:id' element={<DetailCar />} />
          <Route path='/sewa' element={<SewaMobil />} />
          <Route path='/sewa/:id' element={<CarDetail />} />
          <Route path='/tiket' element={<Tiket />} />
        </Route>
        <Route path='/register' element={<Register />} />
        <Route path='/' element={<Login />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
