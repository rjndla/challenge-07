/** @format */

import Navbar from "../Navbar";
import "../../assets/css/DetailCar.css";

import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import User from "../../assets/icon/fi_users.svg";
import Setting from "../../assets/icon/fi_settings.svg";
import Calendar from "../../assets/icon/fi_calendar.svg";
import "../../assets/css/DetailCar.css";
import NumberFormat from "react-number-format";
import ModalImage from "react-modal-image";
import ReactPlayer from "react-player";
import { Link } from "react-router-dom";

const baseURL = "https://rent-car-appx.herokuapp.com/admin/car";
function CarDetail() {
  const { id } = useParams();
  const [cars, setCars] = useState([]);
  useEffect(() => {
    const getCars = async () => {
      const { data: res } = await axios.get(baseURL + "/" + id);
      setCars(res);
    };
    getCars();
  }, [id]);
  return (
    <>
      <Navbar />
      {/* <div className='container'> */}
      <section className='page-detail d-flex justify-content-center'>
        <div className='detail-section mt-4'>
          <div className='row'>
            <div className='package col-auto'>
              <div className='card card-detail mt-2'>
                <p>
                  <strong>Tentang Paket</strong>
                </p>
                <div class='card-body'>
                  <p>Include</p>
                  <ul>
                    <li>
                      Apa saja yang termasuk dalam paket misal durasi max 12 jam
                    </li>
                    <li>Sudah termasuk bensin selama 12 jam</li>
                    <li>Sudah termasuk Tiket Wisata</li>
                    <li>sudah termasuk pajak</li>
                  </ul>
                  <p>Exclude</p>
                  <ul>
                    <li>Tidak termasuk biaya makan sopir Rp.75.000/hari</li>
                    <li>
                      Jika overtime lebih dari 12 jam akan ada tambahan biaya
                      Rp.20.000/jam
                    </li>
                    <li>Tidak termasuk akomodasi penginapan</li>
                  </ul>
                  <p>
                    <strong>Refund, Reschedule, Overtime</strong>
                  </p>
                  <ul>
                    <li>Tidak termasuk biaya makan sopir Rp.75.000/hari</li>
                    <li>
                      Jika overtime lebih dari 12 jam akan ada tambahan biaya
                      Rp.20.000/jam
                    </li>
                    <li>Tidak termasuk akomodasi penginapan</li>
                    <li>Tidak termasuk biaya makan sopir Rp.75.000/hari</li>
                    <li>
                      Jika overtime lebih dari 12 jam akan ada tambahan biaya
                      Rp.20.000/jam
                    </li>
                    <li>Tidak termasuk akomodasi penginapan</li>
                    <li>Tidak termasuk biaya makan sopir Rp.75.000/hari</li>
                    <li>
                      Jika overtime lebih dari 12 jam akan ada tambahan biaya
                      Rp.20.000/jam
                    </li>
                    <li>Tidak termasuk akomodasi penginapan</li>
                  </ul>
                </div>
              </div>
              <div className='button d-flex justify-content-end'>
              <Link to='/tiket'>
                <button type='button' class='btn btn-bayar mt-4'>
                  Lanjutkan Pembayaran
                </button>
              </Link>
              </div>
            </div>

            <div className='col-auto'>
              <div class='card-detail-mobil mt-2'>
                <div class='card-body'>
                  <div className='img-cover'>
                    <ModalImage
                      className='img mr-3 mt-3'
                      small={cars.image}
                      large={cars.image}
                      alt={cars.name}
                      width='100%'
                      height='100%'
                    />
                    <ReactPlayer
                      className='video mt-3 ml-3'
                      url='https://youtu.be/tbjdN8sGa5A'
                      width='100%'
                      height='100%'
                    />
                  </div>
                  <p>
                    <strong>
                      {`${cars.name}`}/{`${cars.category}`}
                    </strong>
                  </p>
                  <div className='icon d-flex'>
                    <p class='card-text'>
                      <img className='me-1' src={User} alt='icon-key' />
                      {`${cars.capacity}`} orang
                    </p>
                    <p class='card-text'>
                      <img className='me-1' src={Setting} alt='icon-clock' />
                      {`${cars.transmission}`}
                    </p>
                    <p class='card-text'>
                      <img className='me-1' src={Calendar} alt='icon-clock' />
                      Tahun {`${cars.year}`}
                    </p>
                  </div>
                  <p>
                    Total
                    <span>
                      <strong>
                        <NumberFormat
                          value={cars.price}
                          displayType={"text"}
                          thousandSeparator={true}
                          prefix={"Rp "}
                        />{" "}
                      </strong>
                    </span>
                  </p>
                  <Link to='/tiket'>
                  <button type='button' class='btn btn-bayar2'>
                    Lanjutkan Pembayaran
                  </button>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* </div> */}
    </>
  );
}

export default CarDetail;
