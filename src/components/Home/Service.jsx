/** @format */

import React from "react";
import ImgService from '../../assets/icon/home/img/img_service.png'

function Service() {
  return (
    <>
      <section id='service'>
        <div class='container'>
          <div class='row'>
            <div class='col-lg-6 col-sm-12'>
              <img
                src={ImgService}
                alt=''
                data-aos='fade-up-right'
                data-aos-duration='1000'
              />
            </div>
            <div class='col-lg-6 col-sm-12'>
              <h2>Best Car Rental for any kind of trip in Jember</h2>
              <p class='info'>
                Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga
                lebih murah dibandingkan yang lain, kondisi mobil baru, serta
                kualitas pelayanan terbaik untuk perjalanan wisata, bisnis,
                wedding, meeting, dll.
              </p>
              <ul class='services-list'>
                <li class='list'>
                  <span>Sewa Mobil Dengan Supir di Bali 12 Jam</span>
                </li>
                <li class='list'>
                  <span>Sewa Mobil Lepas Kunci di Bali 24 Jam</span>
                </li>
                <li class='list'>
                  <span>Sewa Mobil Jangka Panjang Bulanan</span>
                </li>
                <li class='list'>
                  <span>Gratis Antar - Jemput Mobil di Bandara</span>
                </li>
                <li class='list'>
                  <span>Layanan Airport Transfer / Drop In Out</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default Service;
